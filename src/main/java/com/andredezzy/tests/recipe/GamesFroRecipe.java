package com.andredezzy.core.recipe.ingredient;

import com.andredezzy.core.init.Core;
import com.andredezzy.core.item.ItemBuilder;
import com.andredezzy.core.item.handlers.HeadBuilder;
import com.andredezzy.core.recipe.CustomRecipe;
import com.andredezzy.core.recipe.anns.Ingredient;
import com.andredezzy.core.recipe.anns.Result;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class GamesFroRecipe extends CustomRecipe {

    @Ingredient(slot = {0, 2, 4, 6, 8}, color = ChatColor.YELLOW)
    private ItemStack fro;
    @Result
    private ItemStack games;

    public GamesFroRecipe(Core core) {
        super(core);
        this.fro = new ItemBuilder(Material.YELLOW_FLOWER).name("§efrô").build();
        this.games = new HeadBuilder().owner("gameszaum").name("§etotoso do games").build();
    }
}
