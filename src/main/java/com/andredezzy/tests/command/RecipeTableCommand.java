package com.andredezzy.tests.command;

import com.andredezzy.core.command.Command;
import com.andredezzy.core.init.Core;
import com.andredezzy.core.recipe.ingredient.GamesFroRecipe;
import com.andredezzy.tests.Initializer;
import org.bukkit.command.CommandSender;

import java.util.List;

public class RecipeTableCommand extends Command {

    public RecipeTableCommand(Core core) {
        super(core);
    }

    @Override
    public String getName() {
        return "recipetable";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public boolean canBeExecutedByConsole() {
        return false;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        GamesFroRecipe testCustomRecipe = new GamesFroRecipe(Initializer.getInstance());
        sender.sendMessage("");
        sender.sendMessage(testCustomRecipe.getCraft().getItems());
        sender.sendMessage("");
        sender.sendMessage(testCustomRecipe.getCraft().getColors());
        sender.sendMessage("");
        sender.sendMessage(testCustomRecipe.getCraft().getCraftingTable());
    }
}
