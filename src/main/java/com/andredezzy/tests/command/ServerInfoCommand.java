package com.andredezzy.tests.command;

import com.andredezzy.core.command.Command;
import com.andredezzy.core.init.Core;
import com.andredezzy.core.util.machine.Machine;
import com.andredezzy.core.util.machine.ram.Ram;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;

public class ServerInfoCommand extends Command {

    private final Machine machine;

    public ServerInfoCommand(Core core) {
        super(core);
        this.machine = core.getMachine();
    }

    @Override
    public String getName() {
        return "serverinfo";
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("serverinfos", "serverinf");
    }

    @Override
    public String getDescription() {
        return "Get server machine's informations.";
    }

    @Override
    public boolean canBeExecutedByConsole() {
        return true;
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        Ram ram = this.machine.getRam();
        sender.sendMessage("");
        sender.sendMessage("§eInformações do servidor:");
        sender.sendMessage(String.format("Sistema operácional: §a%s §7(V: %s)§f.", this.machine.os, this.machine.osVersion));
        sender.sendMessage(String.format("Arquitetura do sistema: §a%s§f.", this.machine.osArchitecture));
        sender.sendMessage(String.format("Versão do Bukkit: §a%s§f.", Bukkit.getVersion()));
        sender.sendMessage(String.format("TPS dos últimos 1min, 5min, 15min: §a%s§f.", core.getTps(ChatColor.GREEN)));
        sender.sendMessage(String.format("Tempo de atividade: §a%s§f.", this.machine.uptime()));
        sender.sendMessage(String.format("Uso da memória RAM: §a%s%s §7(%s/%s)§f.", ram.percentage(), "%", ram.used(), ram.max()));
        sender.sendMessage(String.format("Gráfico do uso de RAM: §7[%s§7] §e%s%s", ram.graph(), ram.percentage(), "%"));
        sender.sendMessage(String.format("Quantidade de núcleos: §a%s§f.", this.machine.coreAmount));
    }
}
