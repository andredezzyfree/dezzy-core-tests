package com.andredezzy.tests.command;

import com.andredezzy.core.command.Command;
import com.andredezzy.core.init.Core;
import com.andredezzy.core.recipe.ingredient.GamesFroRecipe;
import com.andredezzy.core.recipe.ingredient.IngredientBuilder;
import com.andredezzy.tests.Initializer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ItemsCommand extends Command {

    public ItemsCommand(Core core) {
        super(core);
    }

    @Override
    public String getName() {
        return "testitems";
    }

    @Override
    public List<String> getAliases() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public boolean canBeExecutedByConsole() {
        return false;
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        GamesFroRecipe testRecipe = new GamesFroRecipe(Initializer.getInstance());
        testRecipe.getIngredients().stream().map(IngredientBuilder::getItem).forEach(i -> ((Player) sender)
                .getInventory().addItem(i));
    }
}
