package com.andredezzy.tests;

import com.andredezzy.core.init.Core;
import com.andredezzy.core.recipe.ingredient.GamesFroRecipe;
import com.andredezzy.tests.command.ItemsCommand;
import com.andredezzy.tests.command.RecipeTableCommand;
import com.andredezzy.tests.command.ServerInfoCommand;

public final class Initializer extends Core {

    private static Initializer instance;

    public static Initializer getInstance() {
        return instance;
    }

    @Override
    public void onStartup() {
        instance = this;
    }

    @Override
    public void onRun() {
        getCommandManager().registerCommand(new ItemsCommand(this));
        getCommandManager().registerCommand(new RecipeTableCommand(this));
        getCommandManager().registerCommand(new ServerInfoCommand(this));

        getRecipeManager().registerRecipe(new GamesFroRecipe(this));
    }

    @Override
    public void onStop() {
    }
}
